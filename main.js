// current date and time

function dateTime(timezone) {
  let offset1 = timezone.split("C");
  let offset2 = offset1[1].split(":");
  let offset3 = parseInt(offset2[0]);
  let offset4 = parseInt(offset2[1]);
  let offset = offset3 + (offset4 / 60);
  let x = new Date();
  let month = x.getMonth();
  let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let day = x.getDate();
  let year = x.getFullYear();
  let str1 = ""
  let str = ""
  let str2 = ""
  str1 = day + " " + months[month] + " " + year;
  let localTime = x.getTime();
  let localOffset = x.getTimezoneOffset() * 60000;
  let utc = localTime + localOffset;
  let nd = new Date(utc + (3600000 * offset));
  let h = nd.getHours();
  let m = nd.getMinutes();
  let h1 = "";
  let m1 = "";
  if (h < 10) {
    h1 = h1 + "0" + h.toString();
  }
  if (m < 10) {
    m1 = m1 + "0" + m.toString();
  }
  if (h < 10) {
    if (m < 10) {
      str2 = h1 + ":" + m1;
    }
    else {
      str2 = h1 + ":" + m;
    }
  }
  else {
    if (m < 10) {
      str2 = h + ":" + m1;
    }
    else {
      str2 = h + ":" + m;
    }
  }
  str = str1 + " and " + str2;

  return (str)
}

// fetch data to console
console.log('this is the api')

let mtBtn = document.getElementById("myBtn");
let content = document.getElementById("content");
function getData() {
  url = "https://restcountries.com/v3.1/all";
  fetch(url).then((response) => {
    return response.json();
  }).then((data) => {
    console.log(data);
    let x = data[40]["name"]["common"]
    console.log(x)
  })
}
getData()

//  search
$("#search").on("click", function () {
  let value = $("#input").val();

  $("#countrieslist").empty();

  $.get("https://restcountries.com/v3.1/name/" + value, function (data, status) {
    data.forEach(element => {

      let curr = "NULL";
      if (element.currencies) {
        curr = Object.values(element.currencies)[0].name;
      }
      
      let str = `<div class='box1 mb-4 shadow p-3  bg-body ' style="border:2px solid grey; style="display:flex"">
    <div class="row" style="display:flex" >
            <div class="col-6" style=" display:flex;"> 
              <img src="`+ element.flags.png + `" width="100%" id="imgchg"  />
            </div>
            <div class="col">
              <h2>`+ element.name.common + `</h2>
              <p class="m-0">Currency : `+ curr + `</p>
              <p class="m-0">Current date and time :`+ dateTime(element.timezones[0]) + ` </p>
              
              <a class='btn btn-outline-primary border border-primary border-2' style="border-radius: 0px; margin-bottom: 3%;" href='`+ element.maps.googleMaps + `'>Show Map</a>
              <a class='btn btn-outline-primary border border-primary border-2' style="border-radius: 0px; margin-bottom: 3%;" href='detail.html?country=`+ element.cca3 + `'>Detail</a>
              </div>
          </div></div>`;


      $("#countrieslist").append(str);
    });
  })
})

$.get("https://restcountries.com/v3.1/all", function (data, status) {

  data.forEach(element => {

    let curr = "NULL";
    if (element.currencies) {
      curr = Object.values(element.currencies)[0].name;

    }

    let str = `<div class='box1 mb-4 shadow p-3  bg-body' style="border:1px solid black; style="display:flex"">
    <div class="row" style="display:flex" >
            <div class="col-6" style=" display:flex;"> 
              <img  class="space" src="`+ element.flags.png + `" width="100%" id="imgchg"  />
            </div>
            <div class="col">
              <h2>`+ element.name.common + `</h2>
              <p class="m-0">Currency : `+ curr + `</p>
              <p class="m-0">Current date and time :`+ dateTime(element.timezones[0]) + ` </p>
              <div class="d-flex  justify-content-sm-evenly">
              <div >
              
              <a class='btn btn-outline-primary border border-primary border-2' style="border-radius: 0px; margin-bottom: 3%;" href='`+ element.maps.googleMaps + `'>Show Map</a>
              
              </div>
              <div>
              <a class='btn btn-outline-primary border border-primary border-2' style="border-radius: 0px; margin-bottom: 3%;" href='detail.html?country=`+ element.cca3 + `'>Detail</a>
            </div></div></div>
          </div></div>`;

    $("#countrieslist").append(str);
  });
});
