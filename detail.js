// code for detail
var ur = window.location.href
var myurl = new URL(ur);
var c = myurl.searchParams.get("country");

const url = "https://restcountries.com/v3.1/alpha/" + c;

$.get(url, function (data, status) {

    let str = `
        <h1>`+ data[0].name.common + `</h1>
        <div class="row">
            <div class="col-5">
                <img src="`+ data[0].flags.png + `" width="100%"/>
            </div>
            <div class="col">
                <p class="mb-2"><b>Native Name :</b> </p>
                <p class="mb-2"><b>Capital :</b> `+ data[0].capital[0] + `</p>
                <p class="mb-2"><b>Population :</b> `+ data[0].population + `</p>
                <p class="mb-2"><b>Region :</b> `+ data[0].region + `</p>
                <p class="mb-2"><b>Sub-region</b> : `+ data[0].subregion + `</p>
                <p class="mb-2"><b>Area :</b> `+ data[0].area + `</p>
                <p class="mb-2"><b>Country Code</b> : `+ data[0].region + `</p>
                <p class="mb-2"><b>Languages :</b> `+ data[0].languages + `</p>
                <p class="mb-2"><b>Currencies :</b> </p>
                <p class="mb-2"><b>Timezones :</b> `+ data[0].timezones[0] + `</p>
            </div>
        </div>`;

    $("#data").append(str);



    // for neighbour countries
    let country_neigh = data[0]["borders"]
    console.log()
    if (country_neigh != undefined) {
        let flagval = "";
        console.log("hello")
        for (let i = 0; i < country_neigh.length; i++) {
            
            console.log(country_neigh[i])
            console.log("hello2")
            let flagneigh = "https://restcountries.com/v3.1/alpha/" + country_neigh[i];
            function getneigh() {
                fetch(flagneigh).then((response) => {
                    return response.json();
                    
                }).then((mapdata) => {
                    console.log("check error")
                    let link_flag = mapdata[0]["flags"]["png"];
                    flagval += `
                <img src="`+ link_flag +`" alt="Country Flag"  width="250" height="120"  style=" margin-left:1%;
                margin-right:1%;
                margin-bottom:1%;
                margin-top:1%; border:1px solid black ; " " ">`
                    document.getElementById("data2").innerHTML = flagval;
                    console.log(flagval)
                })

            }
            console.log(flagneigh)
            getneigh()
        }
    }
})




